from django.conf.urls import url, include
from django.views.generic import ListView, DetailView
from questions.models import Question, Answers

urlpatterns = [
    url(r'^$', ListView.as_view(queryset=Question.objects.all(),
                                  template_name ='questions/questions.html')),
    url(r'^(?P<pk>\d+)$', DetailView.as_view(model=Question,
                                   template_name = 'questions/post.html')),
    url(r'^$', ListView.as_view(queryset=Answers.objects.all(),
                                  template_name ='answers/answers.html')),
    url(r'^(?P<pk>\d+)$', DetailView.as_view(model=Answers,
                                   template_name = 'answers/post.html'))
]

