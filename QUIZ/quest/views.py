from django.shortcuts import render


def  home(request):
    return render(request, 'quest/home.html')

def  list(request):
    return render(request, 'quest/list.html')
