from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Example
from .serializers import ExampleSerializer
from django.shortcuts import render
from django.http import HttpResponse
from django.utils.text import compress_string


try:
    from django.conf import settings
    XS_SHARING_ALLOWED_ORIGINS = settings.XS_SHARING_ALLOWED_ORIGINS
    XS_SHARING_ALLOWED_METHODS = settings.XS_SHARING_ALLOWED_METHODS
    XS_SHARING_ALLOWED_HEADERS = settings.XS_SHARING_ALLOWED_HEADERS
    XS_SHARING_ALLOWED_CREDENTIALS = settings.XS_SHARING_ALLOWED_CREDENTIALS
except AttributeError:
    XS_SHARING_ALLOWED_ORIGINS = '*'
    XS_SHARING_ALLOWED_METHODS = ['POST', 'GET', 'OPTIONS', 'PUT', 'DELETE']
    XS_SHARING_ALLOWED_HEADERS = ['Content-Type', '*']
    XS_SHARING_ALLOWED_CREDENTIALS = 'true'

class ExampleList(APIView):

	def get(self, request):
		examle = Example.objects.all()
		serializer = ExampleSerializer(examle, many = True)
		return Response(serializer.data)

	def options(self, request, response):
	    response = http.HttpResponse()
	    response['allow'] = ','.join(['get', 'post', 'put', 'delete', 'options'])
	    response['Access-Control-Allow-Origin'] = XS_SHARING_ALLOWED_ORIGINS
	    response['Access-Control-Allow-Methods'] = ['POST','GET','OPTIONS', 'PUT', 'DELETE']
	    return response	

	def post(self):
		pass

def  add(request):
    return render(request, 'add/add2.html') 